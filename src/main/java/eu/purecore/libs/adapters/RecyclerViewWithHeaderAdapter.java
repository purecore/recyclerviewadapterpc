package eu.purecore.libs.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import eu.purecore.libs.adapters.listeners.IBinder;
import eu.purecore.libs.adapters.listeners.IOnItemClickListener;
import eu.purecore.libs.adapters.listeners.ItemListener;
import eu.purecore.libs.adapters.viewholders.abstracts.CustomViewHolder;

/**
 * Company: Pure Core (office@purecore.eu)
 * Created by Jackq on 2016-01-05.
 * e-mail: jaszczypek@purecore.eu
 */
public class RecyclerViewWithHeaderAdapter<T, THeader, TItemHolder extends CustomViewHolder, TItemHeaderHolder extends CustomViewHolder> extends RecyclerView.Adapter<CustomViewHolder> {
    private Context context;
    private List<T> items;
    private THeader headerData;
    private Class<TItemHolder> itemHolderClass;
    private Class<TItemHeaderHolder> itemHeaderHolderClass;
    private IBinder<T, TItemHolder> binder;
    private IBinder<THeader, TItemHeaderHolder> headerBinder;
    private int layoutItemId;
    private int layoutItemHeaderId;
    private IOnItemClickListener<T> onItemClickListener;
    private IOnItemClickListener<THeader> onItemHeaderClickListener;

    public RecyclerViewWithHeaderAdapter(
            Context context,
            int layoutItemId,
            int layoutItemHeaderId,
            Class<TItemHolder> itemHolderClass,
            Class<TItemHeaderHolder> itemHeaderHolderClass,
            List<T> items,
            THeader headerData,
            IBinder<T, TItemHolder> binder,
            IBinder<THeader, TItemHeaderHolder> binderHeader
    ) {
        this.context = context;
        this.items = items;
        this.itemHeaderHolderClass = itemHeaderHolderClass;
        this.itemHolderClass = itemHolderClass;
        this.headerData = headerData;
        this.binder = binder;
        this.headerBinder = binderHeader;
        this.layoutItemId = layoutItemId;
        this.layoutItemHeaderId = layoutItemHeaderId;
        this.onItemClickListener = null;
        this.onItemHeaderClickListener = null;
    }

    @Override
    public int getItemViewType(int position) {
        return position > 0 ? 1 : 0;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        try {
            if (viewType != 0)
                return this.itemHolderClass.getDeclaredConstructor(View.class)
                        .newInstance(LayoutInflater.from(context).inflate(this.layoutItemId, parent, false));
            else {
                return this.itemHeaderHolderClass.getDeclaredConstructor(View.class)
                        .newInstance(LayoutInflater.from(context).inflate(this.layoutItemHeaderId, parent, false));
            }
        } catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        if ((position) > 0) { //jesli zwykly item wstaw dane
            this.binder.bindHolderToData((TItemHolder) holder, this.items.get(position - 1), position - 1);
            holder.setOnItemClikListener(new ItemListener<>(this.items.get(position - 1), onItemClickListener));
        } else { //header
            this.headerBinder.bindHolderToData((TItemHeaderHolder) holder, this.headerData, position);
            holder.setOnItemClikListener(new ItemListener<>(this.headerData, onItemHeaderClickListener));
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void setOnItemClickListener(IOnItemClickListener<T> listener) {
        this.onItemClickListener = listener;
    }

    public void setOnItemHeaderClickListener(IOnItemClickListener<THeader> onItemHeaderClickListener) {
        this.onItemHeaderClickListener = onItemHeaderClickListener;
    }

    public void add(T s, int position) {
        this.items.add(s);
        notifyItemInserted(position);
    }

    public void addAll(List<T> items) {
        this.items.addAll(items);
        this.notifyDataSetChanged();
    }

    public void clear() {
        this.items.clear();
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return (this.items != null ? this.items.size() + 1 : 1);
    }
}
