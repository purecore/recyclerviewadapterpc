package eu.purecore.libs.adapters.listeners;

/**
 * Company: Pure Core (office@purecore.eu)
 * Created by Jackq on 2015-12-24.
 * e-mail: jaszczypek@purecore.eu
 */
public interface IOnItemClickListener<T> {
    void onItemClick(T data);
}
