package eu.purecore.libs.adapters.listeners;

import android.view.View;

/**
 * Company: Pure Core (office@purecore.eu)
 * Created by Jackq on 2015-12-24.
 * e-mail: jaszczypek@purecore.eu
 */
public class ItemListener<T> implements View.OnClickListener {
    private T data;
    private IOnItemClickListener<T> onItemClickListener;

    public ItemListener(T data, IOnItemClickListener<T> onItemClickListener){
        this.data = data;
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public void onClick(View v) {
        if (onItemClickListener!=null)
            onItemClickListener.onItemClick(data);
    }
}
