package eu.purecore.libs.adapters.listeners;

import eu.purecore.libs.adapters.viewholders.abstracts.CustomViewHolder;

/**
 * Company: Pure Core (office@purecore.eu)
 * Created by Jackq on 2015-12-24.
 * e-mail: jaszczypek@purecore.eu
 */
public interface IBinder<T, T2 extends CustomViewHolder> {
    void bindHolderToData(T2 holder, T data, int position);
}
