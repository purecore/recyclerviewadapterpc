package eu.purecore.libs.adapters.viewholders.abstracts;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;

/**
 * Company: Pure Core (office@purecore.eu)
 * Created by Jackq on 2015-12-24.
 * e-mail: jaszczypek@purecore.eu
 */
public abstract class CustomViewHolder extends RecyclerView.ViewHolder  {

    public CustomViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
    public abstract void setOnItemClikListener(View.OnClickListener onItemClikListener);
}
