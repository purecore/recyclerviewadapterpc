package eu.purecore.libs.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import eu.purecore.libs.adapters.listeners.IBinder;
import eu.purecore.libs.adapters.listeners.IOnItemClickListener;
import eu.purecore.libs.adapters.listeners.ItemListener;
import eu.purecore.libs.adapters.viewholders.abstracts.CustomViewHolder;

/**
 * Company: Pure Core (office@purecore.eu)
 * Created by Jackq on 2016-01-05.
 * e-mail: jaszczypek@purecore.eu
 */
public class RecyclerViewAdapter<T, T2 extends CustomViewHolder> extends RecyclerView.Adapter<T2> {
    private Context context;
    private Class<T2> clazz;
    private List<T> items;
    private IBinder<T, T2> binder;
    private int layoutItemId;
    private IOnItemClickListener<T> onItemClickListener;
    //private CompositeSubscription compositeSubscription;

    public RecyclerViewAdapter(
            Context context,
            Class<T2> clazz,
            int layoutItemId,
            List<T> items,
            IBinder<T, T2> binder
    ) {
        this.context = context;
        this.clazz = clazz;
        this.items = items;
        this.binder = binder;
        this.layoutItemId = layoutItemId;
        this.onItemClickListener = null;
    }
    public RecyclerViewAdapter(
            Context context,
            Class<T2> clazz,
            int layoutItemId,
            IBinder<T, T2> binder
    ) {
        this.context = context;
        this.clazz = clazz;
        this.items = new ArrayList<>();
        this.binder = binder;
        this.layoutItemId = layoutItemId;
        this.onItemClickListener = null;
    }

    @Override
    public T2 onCreateViewHolder(ViewGroup parent, int viewType) {
        try {
            return this.clazz.getDeclaredConstructor(View.class)
                    .newInstance(LayoutInflater.from(context).inflate(this.layoutItemId, parent, false));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onBindViewHolder(T2 holder, int position) {
            this.binder.bindHolderToData(holder, this.items.get(position), position);
            holder.setOnItemClikListener(new ItemListener<>(this.items.get(position), onItemClickListener));
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void setOnItemClickListener(IOnItemClickListener<T> listener) {
        this.onItemClickListener = listener;
    }

    public void add(T s, int position) {
        this.items.add(s);
        notifyItemInserted(position);
    }

    public void addAll(List<T> items) {
        this.items.addAll(items);
        this.notifyDataSetChanged();
    }
    public int getPosition(T item){
        return this.items.indexOf(item);
    }

    public void clear() {
        this.items.clear();
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return (this.items != null ? this.items.size() : 0);
    }
}
